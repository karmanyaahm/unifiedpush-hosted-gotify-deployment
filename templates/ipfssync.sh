#!/bin/bash
PATH="/usr/local/bin:/usr/bin"

date
for i in {{IPFS_PINS_LIST | join(" ")}}
do
	echo $i
	ipfs files mkdir -p /$i
	LOC=$(ipfs resolve -r /ipns/$i) || continue
	echo $LOC
   	ipfs files cp "$LOC" /${i}/$(date +%F_%T)
  	ipfs refs -r $LOC > /dev/null
done

ipfs pin add /ipfs/{{GOTIFY_CID}}
date
